const weeklyForecastCitySelect = document.querySelector(".card__select-city");
const pastForecastCitySelect = document.querySelector(".select-city-past");
const dateInput = document.querySelector(".card__select-date");
const littleCardsFlexContainer = document.querySelector(".card__carousel");
const bigWeatherCardContainer = document.querySelector(".card__big-weather-block");
const weeklyForecastPlaceholder = document.querySelector(".card__weekly-forecast-placeholder");
const pastForecastPlaceholder = document.querySelector(".card__past-forecast-placeholder");
const weeklyForecastBlock = document.querySelector(".card__weather-block");

let cardsDataNumbers = [0, 1, 2];

const iconBaseUrl = `http://openweathermap.org/img/wn/`;
const baseUrl = `https://api.openweathermap.org/data/2.5/onecall`;
const apiKey = `ff521623bbfa89a36503e1bf62252a0f`;
const excludes = `current,minutely,hourly,alerts`;

function setMaxAndMinAttributes(dateInput) {
  const oneDay = 86_400_000;
  const maxDate = new Date().toISOString().slice(0, 10);
  const minDate = new Date(Date.now() - oneDay * 5).toISOString().slice(0, 10);

  dateInput.max = maxDate;
  dateInput.min = minDate;
}

setMaxAndMinAttributes(dateInput);

function getLatAndLon(coordinates) {
  return coordinates.split(", ");
}

function getTemperature(dailyTemperature) {
  const roundedTemperature = Math.round(dailyTemperature);

  if (dailyTemperature > 0) {
    return `+${roundedTemperature}°`;
  }

  return `${roundedTemperature}°`;
}

function getDate(timeStamp) {
  return new Date(timeStamp * 1000)
    .toLocaleString("en", { year: "numeric", month: "short", day: "numeric" })
    .split(", ")
    .join(" ");
}

// Weekly Forecast

function handlingWeeklyForecastData(weeklyForecastResponse) {
  const dailyForecast = weeklyForecastResponse.daily;
  return dailyForecast.map((item, index) => {
    return {
      iconUrl: `${iconBaseUrl}${weeklyForecastResponse.daily[index].weather[0].icon}@2x.png`,
      dailyTemperature: getTemperature(weeklyForecastResponse.daily[index].temp.day),
      date: getDate(weeklyForecastResponse.daily[index].dt),
    };
  });
}

function createWeeklyForecastCards(flexContainer, count, placeholder, container) {
  while (flexContainer.firstChild) {
    flexContainer.removeChild(flexContainer.firstChild);
  }
  placeholder.classList.add("card__placeholder--none-active");
  for (let i = 0; i < count; i++) {
    flexContainer.insertAdjacentHTML(
      "afterbegin",
      `<div class="little-weather-card">
          <div class="little-weather-card__wrapper">
            <div class="little-weather-card__date">
              <time datetime=""></time>
            </div>
            <img class="little-weather-card__img" src="" alt="little_weather_card" />
            <div class="little-weather-card__temperature"></div>
          </div>
        </div>`
    );
  }
  container.querySelector(".card__left-button").classList.remove("card__button--invisible");
  container.querySelector(".card__right-button").classList.remove("card__button--invisible");
}

function setWeeklyForecastCardsValue(weeklyData, cardsDataNumbers) {
  const littleWeatherCards = Array.from(littleCardsFlexContainer.querySelectorAll(".little-weather-card"));

  if (littleCardsFlexContainer.childElementCount > 3) {
    weeklyData.map(function ({ iconUrl, dailyTemperature, date }, index) {
      littleWeatherCards[index].querySelector("img").src = iconUrl;
      littleWeatherCards[index].querySelector(".little-weather-card__temperature").textContent = dailyTemperature;
      littleWeatherCards[index].querySelector("time").textContent = date;
      littleWeatherCards[index].querySelector("time").datetime = `${date}`;
    });
  } else {
    let cardNumber = 0;
    cardsDataNumbers.forEach(number => {
      littleWeatherCards[cardNumber].querySelector("img").src = weeklyData[number].iconUrl;
      littleWeatherCards[cardNumber].querySelector(".little-weather-card__temperature").textContent =
        weeklyData[number].dailyTemperature;
      littleWeatherCards[cardNumber].querySelector("time").textContent = weeklyData[number].date;
      littleWeatherCards[cardNumber].querySelector("time").datetime = `${weeklyData[number].date}`;
      cardNumber++;
    });
  }
}

function showCards(weeklyData) {
  if (window.document.documentElement.clientWidth > 660) {
    createWeeklyForecastCards(littleCardsFlexContainer, 3, weeklyForecastPlaceholder, weeklyForecastBlock);
  } else if (window.document.documentElement.clientWidth < 660) {
    createWeeklyForecastCards(littleCardsFlexContainer, 7, weeklyForecastPlaceholder, weeklyForecastBlock);
  }

  window.onresize = function () {
    if (littleCardsFlexContainer.childElementCount == 7 && window.document.documentElement.clientWidth > 660) {
      cardsDataNumbers = [0, 1, 2];
      createWeeklyForecastCards(littleCardsFlexContainer, 3, weeklyForecastPlaceholder, weeklyForecastBlock);
    } else if (littleCardsFlexContainer.childElementCount == 3 && window.document.documentElement.clientWidth < 660) {
      createWeeklyForecastCards(littleCardsFlexContainer, 7, weeklyForecastPlaceholder, weeklyForecastBlock);
    }
    setWeeklyForecastCardsValue(weeklyData, cardsDataNumbers);
  };
}

async function getWeeklyForecast(lat, lon) {
  try {
    const forecastRequest = await fetch(
      `${baseUrl}?&lat=${lat}&lon=${lon}&exclude=${excludes}&units=metric&appid=${apiKey}`
    );
    const response = await forecastRequest.json();
    const weeklyData = handlingWeeklyForecastData(response);
    showCards(weeklyData);

    setWeeklyForecastCardsValue(weeklyData, cardsDataNumbers);

    weeklyForecastBlock.addEventListener("click", function (element) {
      if (element.target.alt == "right-arrow" && cardsDataNumbers[cardsDataNumbers.length - 1] !== 7) {
        cardsDataNumbers = cardsDataNumbers.map(item => ++item);
        setWeeklyForecastCardsValue(weeklyData, cardsDataNumbers);
      } else if (element.target.alt == "left-arrow" && cardsDataNumbers[0] !== 0) {
        cardsDataNumbers = cardsDataNumbers.map(item => --item);
        setWeeklyForecastCardsValue(weeklyData, cardsDataNumbers);
      }
    });
  } catch (error) {
    console.error(error.message);
  }
}

weeklyForecastCitySelect.addEventListener("change", function () {
  const [lat, lon] = getLatAndLon(weeklyForecastCitySelect.value);

  getWeeklyForecast(lat, lon);
});

// Past Forecast
const pastForecastData = {
  lat: null,
  lon: null,
  pastDate: null,
};

function setPastForecastData(pastForecastData, targetElement, dateValue) {
  if (targetElement === pastForecastCitySelect) {
    const [lat, lon] = getLatAndLon(pastForecastCitySelect.value);
    pastForecastData.lat = lat;
    pastForecastData.lon = lon;
  } else if (targetElement === dateInput) {
    pastForecastData.pastDate = `${Math.round(new Date(dateValue).getTime() / 1000.0)}`;
  }
}

function isFillingPastData(pastForecastData) {
  return !Object.values(pastForecastData).some(value => value === null);
}

function setPastForecastValues(pastForecastResponse, bigCardContainer, dateToChange) {
  if (pastForecastResponse.cod !== 404 && pastForecastResponse.cod !== 400) {
    createPastForecastCard(bigCardContainer, pastForecastPlaceholder);

    const convertedDate = changeDateOrder(dateToChange);
    bigCardContainer.querySelector("time").dateTime = convertedDate;
    bigCardContainer.querySelector("time").textContent = convertedDate;

    bigCardContainer.querySelector("img").src = `${iconBaseUrl}${pastForecastResponse.current.weather[0].icon}@2x.png`;

    bigCardContainer.querySelector(".big-weather-card__temperature").textContent = getTemperature(
      pastForecastResponse.current.temp
    );
  }
}

function changeDateOrder(date) {
  const daysDifference = new Date().getDate() - new Date(date).getDate();

  if (daysDifference <= 5) {
    return new Date(date)
      .toLocaleString("en", { month: "short", day: "numeric", year: "numeric" })
      .split(", ")
      .join(" ");
  }
}

function createPastForecastCard(bigCardContainer, pastPlaceholder) {
  if (bigCardContainer.childElementCount === 0) {
    pastPlaceholder.classList.add("card__placeholder--none-active");
    bigCardContainer.insertAdjacentHTML(
      "afterbegin",
      `<div class="big-weather-card">
        <div class="big-weather-card__wrapper">
          <div class="big-weather-card__date">
            <time datetime=""></time>
          </div>
          <img class="big-weather-card__img" src="" alt="big-weather-card" />
          <div class="big-weather-card__temperature"></div>
        </div>
      </div>`
    );
  }
}

async function getPastForecast({ lat, lon, pastDate }) {
  try {
    const pastForecastRequest = await fetch(
      `${baseUrl}/timemachine?lat=${lat}&lon=${lon}&dt=${pastDate}&appid=${apiKey}&units=metric`
    );

    return pastForecastRequest.json();
  } catch (error) {
    console.error(error.message);
  }
}

pastForecastCitySelect.addEventListener("change", function (element) {
  setPastForecastData(pastForecastData, element.target, dateInput.value);
  const isFilled = isFillingPastData(pastForecastData);

  if (isFilled) {
    const pastForecastResponse = getPastForecast(pastForecastData);
    pastForecastResponse.then(pastForecastResponseData => {
      setPastForecastValues(pastForecastResponseData, bigWeatherCardContainer, dateInput.value);
    });
  }
});

dateInput.addEventListener("change", function (element) {
  setPastForecastData(pastForecastData, element.target, dateInput.value);
  const isFilled = isFillingPastData(pastForecastData);

  if (isFilled) {
    const pastForecastResponse = getPastForecast(pastForecastData);
    pastForecastResponse.then(pastForecastResponseData => {
      setPastForecastValues(pastForecastResponseData, bigWeatherCardContainer, dateInput.value);
    });
  }
});
